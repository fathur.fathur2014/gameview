<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "personnel";
$conn = new mysqli($servername, $username,
	$password,
	$dbname);
/* check connection */
if ($conn->connect_error) {
	die("Connection failed: " . $conn->connect_error);
}
$callback = $_REQUEST['callback'];
$records = json_decode($_REQUEST['records']);
$barID= $records->{"barID"};
$name = $records->{"name"};
$g1 = $records->{"g1"};
$g2 = $records->{"g2"};
$g3 = $records->{"g3"};
$g4 = $records->{"g4"};
$g5 = $records->{"g5"};
$g6 = $records->{"g6"};
$success = 'false';
$error = 'no error';
// Create the output object.
$output = array();
$query="update bar set
name='$name',g1='$g1',g2='$g2',g3='$g3',g4='$g4',g5='$g5',g6='$g6' where
barID=$barID";
if ($conn->query($query) === TRUE) {
	$success = 'true';
}
else{
	$error = $conn->error;
}
//start output
if ($callback) {
	header('Content-Type: text/javascript');
	echo $callback . '({"success":'.$success.',
	"message":"'.$error.'"});';
} else {
	header('Content-Type: application/x-json');
	echo json_encode($output);
}
$conn->close();
?>