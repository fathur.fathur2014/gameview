<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "personnel";
$conn = new mysqli($servername, $username,
	$password,
	$dbname);
/* check connection */
if ($conn->connect_error) {
	die("Connection failed: " . $conn->connect_error);
}
$callback = $_REQUEST['callback'];
$records = json_decode($_REQUEST['records']);
$name = $records->{"name"};
$email = $records->{"email"};
$phone = $records->{"phone"};
// Create the output object.
$output = array();
$success = 'false'; 
$query="insert into user (name,email,phone) values ('$name','$email','$phone')";
if ($conn->query($query) === TRUE) {
	$success = 'true';
}
else{
	$success = 'false';
	$error = $conn->error;
}
//start output
if ($callback) {
	header('Content-Type: text/javascript');
	echo $callback . '({"success":'.$success.', "items":' .
	json_encode($output) . '});';
} else {
	header('Content-Type: application/x-json');
	echo json_encode($output);
}
$conn->close();
?>