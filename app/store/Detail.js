Ext.define('Latihan.store.Detail', {
    extend: 'Ext.data.Store',
    
    alias: 'store.detail',
    storeId:'detail',
    //autoLoad: true,
    //autoSync: true,
    fields: [
        'user_id','name', 'email', 'phone'
    ],

    proxy: {
        type: 'jsonp',
        api:{
            read   : Latihan.util.Globals.getPhppath()+"/MyApp_php/readDetail.php",

            //read: "http://localhost/MyApp_php/readDetail.php"
        },
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
 //    listeners: {
 //         beforeload: function(store, operation, eOpts){
 //            this.getProxy().setExtraParams({
 //                 user_id: localStorage.getItem('user_id')
 //         });
 //     }
 // }
});
