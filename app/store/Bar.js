Ext.define('Latihan.store.Bar', {
        extend: 'Ext.data.Store',
        alias: 'store.bar',
        autoLoad:true,
        autoSync:true,
        fields: ['barID', 'g1', 'g2', 'g3', 'g4', 'g5', 'g6', 'name'],
        proxy: {
        type: 'jsonp',
        api:{
            read   : Latihan.util.Globals.getPhppath()+"/MyApp_php/readBar.php",
            update   : Latihan.util.Globals.getPhppath()+"/MyApp_php/updateBar.php",

            // read: "http://localhost/MyApp_php/readBar.php",
            // update: "http://localhost/MyApp_php/updateBar.php",
        },
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
});