Ext.define('Latihan.store.Personnel', {
    extend: 'Ext.data.Store',
    storeId:'personnel',
    alias: 'store.personnel',
    autoLoad: true,
    autoSync: true,
    fields: [
        'user_id','name', 'email', 'phone'
    ],
    requires: [
        "Latihan.util.Globals"
    ],

    proxy: {
        type: 'jsonp',
        api:{
            read   : Latihan.util.Globals.getPhppath()+"/MyApp_php/readPersonnel.php",
            update : Latihan.util.Globals.getPhppath()+"/MyApp_php/updatePersonnel.php",
            destroy: Latihan.util.Globals.getPhppath()+"/MyApp_php/destroyPersonnel.php",
            create : Latihan.util.Globals.getPhppath()+"/MyApp_php/createPersonnel.php"


            // read: "http://localhost/MyApp_php/readPersonnel.php",
            // update: "http://localhost/MyApp_php/updatePersonnel.php",
            // destroy: "http://localhost/MyApp_php/destroyPersonnel.php",
            // create: "http://localhost/MyApp_php/createPersonnel.php"
        },
        reader: {
            type: 'json',
            rootProperty: 'items',
            messageProperty: 'error'
        }
    }

//     listeners: {
//         beforeload: function(store, operation, eOpts){
//             this.getProxy().setExtraParams({
//                 user_id: localStorage.getItem('user_id')
//         });
//     }
// }
});
