/**
 * This view is an example list of people.
 */
Ext.define('Latihan.view.main.ListBar', {
    extend: 'Ext.grid.Grid',
    xtype: 'listbar',

    requires: [
        'Latihan.store.Personnel',
        'Ext.grid.plugin.Editable',
    ],

    plugins: [{
        type: 'grideditable'
    }],

    title: 'Bar',

    // store: {
    //     type: 'personnel'
    // },
    bind: '{bar}',
    viewModel: {
        stores:{
            bar: {
                type: 'bar'
            }
        }
    },   
    columns: [
        { text: 'Name',  dataIndex: 'name', flex:3 , editable: true},
        { text: 'g1', dataIndex: 'g1', flex:2, editable: true },
        { text: 'g2', dataIndex: 'g2', flex:2, editable: true },
        { text: 'g3', dataIndex: 'g3', flex:2, editable: true },
        { text: 'g4', dataIndex: 'g4', flex:2, editable: true },
        { text: 'g5', dataIndex: 'g5', flex:2, editable: true },
        { text: 'g6', dataIndex: 'g6', flex:2, editable: true }
    ],

    listeners: {
        select: 'onBarItemSelected'
    }
});
