/**
 * This view is an example list of people.
 */
Ext.define('Latihan.view.main.List', {
    extend: 'Ext.grid.Grid',
    xtype: 'mainlist',

    requires: [
        'Latihan.store.Personnel',
        'Ext.grid.plugin.Editable',
    ],

    plugins: [{
        type: 'grideditable'
    }],

    title: 'Personnel',

    // store: {
    //     type: 'personnel'
    // },
    bind: '{personnel}',
    viewModel: {
        stores:{
            personnel: {
                type: 'personnel'
            }
        }
    },   
    columns: [
        { text: 'Name',  dataIndex: 'name', width: 100 , editable: true},
        { text: 'Email', dataIndex: 'email', width: 230 },
        { text: 'Phone', dataIndex: 'phone', width: 150, editable: true }
    ],

    listeners: {
        select: 'onItemSelected'
    }
});
