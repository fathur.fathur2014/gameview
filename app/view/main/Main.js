/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting causes an instance of this class to be created and
 * added to the Viewport container.
 *
 * TODO - Replace the content of this view to suit the needs of your application.
 */
Ext.define('Latihan.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',

    requires: [
        'Ext.MessageBox',
        
        'Latihan.view.main.MainController',
        'Latihan.view.main.MainModel',
        'Latihan.view.main.List',
        'Latihan.store.Bar',
        'Latihan.view.main.BasicDataView',
        'Latihan.view.main.FormPanel',
        'Latihan.view.main.Bar',
        'Latihan.view.main.ListBar',
        "Latihan.util.Globals" 
    ],

    controller: 'main',
    viewModel: 'main',
    store:{
        type:'personnel',
    },
    defaults: {
        tab: {
            iconAlign: 'top'
        },
        styleHtmlContent: true
    },

    tabBarPosition: 'bottom',

    items: [
        {
            xtype: 'toolbar',
            docked: 'top',
            items: [
                {
                    text: 'READ',
                    ui: 'action',
                    scope: this,
                    listeners:{
                        tap: 'onReadClicked'
                    }
                }
            ]
        },{
            title: 'Home',
            iconCls: 'x-fa fa-home',
            layout: 'fit',
            // The following grid shares a store with the classic version's grid as well!
            items: [{
                xtype: 'panel',
                layout: 'hbox',
                items:[{
                    xtype: 'mainlist',
                    flex: 2
                    
                },{
                    xtype: 'detail',
                    flex: 1
                },{
                    xtype: 'editform',
                    flex: 1 
                }]
            }]
        },{
            title: 'Charts',
            iconCls: 'x-fa fa-user',
            layout: 'fit',
            // The following grid shares a store with the classic version's grid as well!
            items: [{
                xtype: 'panel',
                layout: 'vbox',
                items:[{
                    xtype: 'bar',
                    flex: 1
                    
                },{
                    xtype: 'listbar',
                    flex: 1
                }]
            }]
        },{
            title: 'Groups',
            iconCls: 'x-fa fa-users',
            xtype:'bar'
        },{
            title: 'Settings',
            iconCls: 'x-fa fa-cog',
            bind: {
                html: '{loremIpsum}'
            }
        }
    ]
});
