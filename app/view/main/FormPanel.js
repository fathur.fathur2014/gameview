Ext.define('Latihan.view.main.FormPanel', {
    extend: 'Ext.form.Panel',

    shadow: true,
    xtype:'editform',
    id: 'editform',
    items: [
                {
                    xtype: 'textfield',
                    name: 'name',
                    id:'myname',
                    label: 'Name',
                    placeHolder: 'Your Name',
                    autoCapitalize: true,
                    required: true,
                    clearIcon: true
                },
                {
                    xtype: 'emailfield',
                    name: 'email',
                    id:'myemail',
                    label: 'Email',
                    placeHolder: 'me@mail.com',
                    clearIcon: true
                },
                {
                    xtype: 'textfield',
                    name: 'phone',
                    id:'myphone',
                    label: 'Phone',
                    placeHolder: '08------',
                    //autoCapitalize: true,
                    required: true,
                    clearIcon: true
                },{
                    xtype:'button',
                    ui: 'action',
                    text:'Simpan',
                    handler: 'onSimpanPerubahan'
                },
                {
                    xtype:'button',
                    ui: 'confirm',
                    text:'Tambah Data',
                    handler: 'onTambahPersonnel'
                },{
                    label: "Unggah Audio",
                    html : "Rekam Percakapan : "+
                            '<div id="controls">' +
                                '<button id="recordCordova"> Mulai Merekam </button>' +
                                '<div id="recordingInfo"></div><br>'
                }
            ]
});