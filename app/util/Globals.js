Ext.define("Latihan.util.Globals",{
	singleton: true,
	alternateClassName: 'globalUtils',
	version:'1.0',
	config:{
		phppath:'https://fathurdpm.000webhostapp.com/Latihan/resources'
		//phppath:'http://localhost/Latihan/resources'
	},
	constructor : function(config) {
		this.initConfig(config);
	},

	startRecordCordova: function(){
		let opts = {limit:1}
		navigator.device.capture.captureAudio(globalUtils.captureSuccess, globalUtils.captureError, opts);

	},

	captureSuccess: function(mediaFiles){
		var i, path, len;
		name = mediaFiles[0].name;
		fileURL = mediaFiles[0].fullPath;
		type = mediaFiles[0].type;
		size = mediaFiles[0].size;
		if (size > 80000) {
			document.getElementById('recordingInfo').textContent = "Rekaman Anda Terlalu Panjang, Coba Lagi..";
		}
		else{
			document.getElementById('recordingInfo').textContent = "Menyimpan Rekaman Suara..";

			var uri = encodeURI("https://fathurdpm.000webhostapp.com/Latihan/resources/MyApp_php/upload.php");
			var options = new FileUploadOptions();
			options.filekey="file";
			var form = Ext.getCmp('tambahkata').getValue();
			var filename = "rekaman.m4a";
			localStorage.setItem('audiotype','m4a');
			options.fileName = filename;
			options.mimeType = "text/plain";
			var params = {};
			params.user = 'admin';
			params.npm = '183510340';
			options.params = params;
			var ft = new FileTransfer();
			ft.upload(fileURL, uri, globalUtils.uploadSuccess, globalUtils.uploadError, options);
		}
	},

	captureError: function(error){
		document.getElementById('recordingInfo').textContent = 'Error Code: ' + error.code;
	},

	uploadSuccess: function(r){
		if (r.response=="failed") {
			document.getElementById('recordingInfo').textContent = "Rekaman Gagal Disimpan";
		}
		else{
			documnet.getElementById('recordingInfo').textContent = "Rekaman Berhasil Disimpan";
		}
	},

	uploadError: function(error){
		Ext.Msg.Alert("An Error Has Occured : Code = " + error.code);
	}
});